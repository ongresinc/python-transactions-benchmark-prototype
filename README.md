# tx-benchmark

Benchmark for MongoDB and PostgreSQL with transaction support.

## Usage


```
usage: txbench.py [-h] [--host HOST] -D DURATION -t TEST -c
                  {mongo,mongodb,postgres,postgresql,pg} [-d DATABASE]
                  [-U USER] [-i INTERVAL] [-p PARALLEL] [-P PASSWORD]
                  [-m MOVEMENTS]

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           Host to connect
  -D DURATION, --duration DURATION
                        Duration (in seconds) for the test to run
  -t TEST, --test TEST  Name of the test to run
  -c {mongo,mongodb,postgres,postgresql,pg}, --database-class {mongo,mongodb,postgres,postgresql,pg}
                        Type of test [mongo | postgres]
  -d DATABASE, --database DATABASE
                        Name of the database
  -U USER, --user USER  Database user
  -i INTERVAL, --interval INTERVAL
                        Checkpoint interval, in seconds (default 1sec)
  -p PARALLEL, --parallel PARALLEL
                        How many connections running in parallel
  -P PASSWORD, --password PASSWORD
                        unsecure way to pass password
  -m MOVEMENTS, --movements MOVEMENTS
                        Amount of operations for the test
```

`txbench` supports several (ideally equivalent) benchmarks for _MongoDB_ and for _PostgreSQL_. The code for the tests resides in the `mongo_tests.py` and `postgresql_tests.py` modules. Feel free to write your own tests and happy _benchmarking_!


## Docker

```sh
cd docker/
docker-compose build 
~/ongres/repos/tx-benchmark/docker (master ✘)✹ ᐅ docker-compose down ; docker-compose up -d  ; sleep 15 ; pr  -w200 -m <(docker-compose logs appmg) <(docker-compose logs apppg )

Stopping docker_postgres_1 ... done
Stopping docker_mongo_1    ... done
Removing docker_apppg_1    ... done
Removing docker_appmg_1    ... done
Removing docker_postgres_1 ... done
Removing docker_mongo_1    ... done
Removing network docker_default
Creating network "docker_default" with the default driver
Creating docker_mongo_1    ... done
Creating docker_postgres_1 ... done
Creating docker_apppg_1    ... done
Creating docker_appmg_1    ... done


May 14 22:13 2019  Page 1


Attaching to docker_appmg_1									    Attaching to docker_apppg_1
appmg_1	 | 2019-05-15 01:13:14,Thread-3,1,5,0.009199,51				    apppg_1     | 2019-05-15 01:13:17,Thread-2,1,5,0.004769,274
appmg_1	 | 2019-05-15 01:13:14,Thread-2,1,5,0.015005,32				    apppg_1     | 2019-05-15 01:13:17,Thread-3,1,5,0.004542,288
appmg_1	 | 2019-05-15 01:13:14,Thread-1,1,5,0.010071,47				    apppg_1     | 2019-05-15 01:13:17,Thread-1,1,5,0.004626,283
appmg_1	 | 2019-05-15 01:13:15,Thread-1,1,5,0.010348,81				    apppg_1     | 2019-05-15 01:13:18,Thread-2,1,5,0.004102,241
appmg_1	 | 2019-05-15 01:13:15,Thread-3,1,5,0.010986,79				    apppg_1     | 2019-05-15 01:13:18,Thread-3,1,5,0.004648,213
appmg_1	 | 2019-05-15 01:13:15,Thread-2,1,5,0.010177,84				    apppg_1     | 2019-05-15 01:13:18,Thread-1,1,5,0.004087,242
appmg_1	 | 2019-05-15 01:13:16,Thread-3,1,5,0.011224,76				    apppg_1     | 2019-05-15 01:13:19,Thread-2,1,5,0.006378,156
appmg_1	 | 2019-05-15 01:13:16,Thread-1,1,5,0.012739,69				    apppg_1     | 2019-05-15 01:13:19,Thread-1,1,5,0.005604,177
appmg_1	 | 2019-05-15 01:13:16,Thread-2,1,5,0.012365,70				    apppg_1     | 2019-05-15 01:13:19,Thread-3,1,5,0.004832,206
appmg_1	 | 2019-05-15 01:13:17,Thread-3,1,5,0.016691,51				    apppg_1     | 2019-05-15 01:13:20,Thread-3,1,5,0.004883,202
appmg_1	 | 2019-05-15 01:13:17,Thread-2,1,5,0.020793,41				    apppg_1     | 2019-05-15 01:13:20,Thread-2,1,5,0.006288,158
appmg_1	 | 2019-05-15 01:13:17,Thread-1,1,5,0.015229,57				    apppg_1     | 2019-05-15 01:13:20,Thread-1,1,5,0.005592,178
appmg_1	 | 2019-05-15 01:13:18,Thread-1,1,5,0.017694,48				    apppg_1     | 2019-05-15 01:13:21,Thread-3,1,5,0.004812,208
appmg_1	 | 2019-05-15 01:13:18,Thread-3,1,5,0.020179,44				    apppg_1     | 2019-05-15 01:13:21,Thread-1,1,5,0.005525,181
appmg_1	 | 2019-05-15 01:13:18,Thread-2,1,5,0.021734,40				    apppg_1     | 2019-05-15 01:13:21,Thread-2,1,5,0.006484,155
appmg_1	 | 2019-05-15 01:13:19,Thread-3,1,5,0.016153,53				    apppg_1     | 2019-05-15 01:13:22,Thread-2,1,5,0.003499,280
appmg_1	 | 2019-05-15 01:13:19,Thread-2,1,5,0.018058,48				    apppg_1     | 2019-05-15 01:13:22,Thread-1,1,5,0.003584,274
appmg_1	 | 2019-05-15 01:13:19,Thread-1,1,5,0.014819,58				    apppg_1     | 2019-05-15 01:13:22,Thread-3,1,5,0.003602,273
appmg_1	 | 2019-05-15 01:13:20,Thread-1,1,5,0.014346,59				    apppg_1     | 2019-05-15 01:13:23,Thread-2,1,5,0.003735,265
appmg_1	 | 2019-05-15 01:13:20,Thread-2,1,5,0.018527,46				    apppg_1     | 2019-05-15 01:13:23,Thread-1,1,5,0.002883,343
appmg_1	 | 2019-05-15 01:13:20,Thread-3,1,5,0.016386,54				    apppg_1     | 2019-05-15 01:13:23,Thread-3,1,5,0.00344,288
appmg_1	 | 2019-05-15 01:13:21,Thread-2,1,5,0.017374,50				    apppg_1     | 2019-05-15 01:13:24,Thread-1,1,5,0.0027,365
appmg_1	 | 2019-05-15 01:13:21,Thread-3,1,5,0.01613,53				    apppg_1     | 2019-05-15 01:13:24,Thread-2,1,5,0.003717,267
appmg_1	 | 2019-05-15 01:13:21,Thread-1,1,5,0.014617,59				    apppg_1     | 2019-05-15 01:13:24,Thread-3,1,5,0.003458,287

```

### For testing

Mongo and Postgres services should be up.


```
 docker-compose down ;  docker-compose up -d mongo ; docker-compose up -d postgres
```

```
docker-compose build apppg # Build for both engines
docker-compose up apppg    # or appmg
```

