import pymongo
import datetime


class MongoCollection(object):
    def __init__(self, database, name):
        self.name = name
        self.database = database

    # def create(self,create=False):
    #     self.collection = pymongo.collection.Collection(database=self.database, name=self.name, create=create)
    def create(self):
        self.collection = pymongo.collection.Collection(database=self.database, name=self.name, create=False)

    def insert(self, value,session=None):
        self.collection.insert_one(value, session)

    def get(self, filter=None):
        return self.collection.find_one()
    
    def getAll(self):
        return self.collection.find()

    def updateOne(self, filter, newvalues,upsert=True):
        return self.collection.update_one(filter, newvalues,upsert)

    def drop(self):
        self.collection.drop()
    
    def createIndex(self,key):
        self.collection.create_index(key)
    


if __name__ == '__main__':
    pass

