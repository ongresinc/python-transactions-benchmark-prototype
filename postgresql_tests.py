import psycopg2
import random
import datetime
import time
import json
import threading
import sys


class TXWrapper(object):
    
  def __init__(self, func, *args, **kwargs):
    self._func = func
    self._args = args
    self._kwargs = kwargs
    return None

  def __call__(self):
    return self._func(*self._args, **self._kwargs)


def InitiateDDL(client):
  """
  Initialize tables
  See https://www.postgresql.org/message-id/CA+TgmoZAdYVtwBfp1FL2sMZbiHCWT4UPrzRLNnX1Nb30Ku3-gg@mail.gmail.com
  """
  cur = client.cursor()
  ddl = """
    CREATE TABLE IF NOT EXISTS accounts (account_id integer primary key, balance float);
    CREATE TABLE IF NOT EXISTS audit (audit_recno serial, msg jsonb );
  """
  cur.execute(ddl)
  client.commit()
  return

def TransactionTest(client,movements):
  account_id = random.randint(1,5000)
  insert_accounts_template = "insert into accounts values ('{}', '{}') ON CONFLICT (account_id) DO UPDATE set balance = accounts.balance + {}"
  insert_audit_template = "insert into audit (msg) values ('{}')"
  
  t = time.time()
  conn = client.cursor()
  for m in range(movements):
    amount = random.randint(1, 100)
    conn.execute(insert_accounts_template.format(account_id,amount,amount))
  msg = {"account_id": account_id, "amount": amount, "date": str(datetime.datetime.utcnow())}
  conn.execute(insert_audit_template.format(json.dumps(msg)))
  
  client.commit()
  return (time.time() - t)


def run(test_name, args, event,initiate=False):
    header = "timestamp,thread#,totalThreads,interval,movements,timepertx,number_of_tx"
    user = 'postgres' if args.user is None else args.user
    connString = "dbname='{}' user='{}' host='{}' password='{}'".format(args.database,user,args.host,args.password)
    client = psycopg2.connect(connString)
    if initiate:
      InitiateDDL(client)
      event.set()
    
    event.wait(timeout=20)

    theTest = TXWrapper(test_name,client,args.movements)    
    startTime = round(time.time())

    while time.time() < startTime + args.duration: ## global loop
      tx = 0
      t0 = round(time.time())
      accTxTime = 0
      while time.time() < t0 + args.interval:  # One second period
        accTxTime = accTxTime + theTest()
        tx = tx + 1
      print ("{},{},{},{},{},{},{}".format(datetime.datetime.fromtimestamp(t0).strftime('%Y-%m-%d %H:%M:%S'), 
                                            threading.currentThread().getName(),
                                            args.parallel,args.interval,args.movements, 
                                            round(accTxTime/tx,6), tx ))
    client.close()
