from mongo import MongoCollection

import random
import datetime
import pymongo
import time
import threading
import timeit


class TXWrapper(object):
    
  def __init__(self, func, *args, **kwargs):
    self._func = func
    self._args = args
    self._kwargs = kwargs
    return None

  def __call__(self):
    return self._func(*self._args, **self._kwargs)


def initiateIX(db):
  accounts_collection = MongoCollection(db, 'accounts' )
  accounts_collection.createIndex('account_id')
  accounts_collection.create()
  audit_collection = MongoCollection(db,'audit')
  audit_collection.create()

def TransactionTest(session, db,movements):
  accounts_collection = MongoCollection(db, 'accounts')
  accounts_collection.create()
  audit_collection = MongoCollection(db,'audit')
  audit_collection.create()
  # accounts_collection.createIndex('account_id')
  account_id = random.randint(1,5000)

  t = time.time()
  session.start_transaction()
  for m in range(movements):
    amount = random.randint(1, 100)
    accounts_collection.updateOne({ "account_id" : account_id}, { "$inc" : { "count" : amount}}, upsert=True)
  audit_collection.insert({"account_id": account_id, "amount": amount, "date": datetime.datetime.utcnow()}, session=session)
  
  session.commit_transaction()
  return (time.time() - t)


def run(test_name, args,event,inititate=False):
    header = "timestamp,thread#,totalThreads,interval,movements,timepertx,number_of_tx"
    connStringM = "mongodb://{}:27017/".format(args.host) 
    clientM = pymongo.MongoClient(host=connStringM)
    dbM = clientM["DB"]

    connString = "mongodb://{}:27017/?replicaSet=myreplica&retryWrites=true".format(args.host) 
    client = pymongo.MongoClient(host=connString)
    db = client["DB"]

    if inititate:
      # If tries to initiate when it is already initiated, breaks, so 
      # we wrap this
      try:
        clientM.admin.command("replSetInitiate")
        initiateIX(db)
        clientM.close()
      except:
        pass
      event.set()
    
    event.wait(timeout=20)
    
    with client.start_session() as session:
      theTest = TXWrapper(test_name,session, db, args.movements)
      """
      Initialize Collection by inserting the first documents outside
      the timing.
      """
      theTest()
      startTime = round(time.time())
      while time.time() < startTime + args.duration: ## global loop
        tx = 0
        t0 = round(time.time())
        accTxTime = 0
        while time.time() < t0 + args.interval:  # One second period per default
          accTxTime = accTxTime + theTest()
          tx = tx + 1
        print ("{},{},{},{},{},{},{}".format(datetime.datetime.fromtimestamp(t0).strftime('%Y-%m-%d %H:%M:%S'), 
                                            threading.currentThread().getName(),
                                            args.parallel,args.interval,args.movements, round(accTxTime/tx,6), tx ))
    client.close()
    
        